//
//  AppCoordinator.swift
//  RicTy
//
//  Created by AliReza on 2022-11-27.
//

import UIKit
import Combine

class AppCoordinator: AppCoordinatorProtocol {
    
    var didFinish: PassthroughSubject<Coordinator, Never>
    
    var type: CoordinatorType
    
    var navigationController: UINavigationController
    
    var childCoordinators: [Coordinator]
    
    var cancellableStorage: Set<AnyCancellable>
    
    required init(type: CoordinatorType, navigationController: UINavigationController) {
        didFinish = PassthroughSubject()
        self.type = type
        self.navigationController = navigationController
        childCoordinators = []
        cancellableStorage = []
    }
    
    func start() {
        
        if UserDefaultsManager.shared.isFirstLaunch() {
            showOnBoardingFlow()
        } else {
            showMainFlow()
        }
    }
    
    func showOnBoardingFlow() {
        let coorinator = OnBoardingCoordinator(type: .onBoarding, navigationController: navigationController)
        coorinator.didFinish.sink { [weak self] recievedCoordinator in
            self?.sinkWithCoordinator(recievedCoordinator)
        }.store(in: &cancellableStorage)
        childCoordinators.append(coorinator)
        coorinator.start()
        return
    }
    
    func showLoginFlow() {
        let coorinator = LoginCoordinator(type: .login, navigationController: navigationController)
        coorinator.didFinish.sink { [weak self] recievedCoordinator in
            self?.sinkWithCoordinator(recievedCoordinator)
        }.store(in: &cancellableStorage)
        childCoordinators.append(coorinator)
        coorinator.start()
        return
    }
    
    func showMainFlow() {
        let coorinator = TabBarCoordinator(type: .tab, navigationController: navigationController)
        coorinator.didFinish.sink { [weak self] recievedCoordinator in
            self?.sinkWithCoordinator(recievedCoordinator)
        }.store(in: &cancellableStorage)
        childCoordinators.append(coorinator)
        coorinator.start()
        return
    }
    
    func sinkWithCoordinator(_ coordinator: Coordinator) {
        childCoordinators = childCoordinators.filter({ $0.type != coordinator.type })
        
        switch coordinator.type {
            
        case .onBoarding:
            navigationController.viewControllers.removeAll()
            showLoginFlow()
            
        case .login:
            navigationController.viewControllers.removeAll()
            showMainFlow()
            
        case .tab:
            navigationController.viewControllers.removeAll()
            showLoginFlow()
            
        default:
            break
        }
        
    }
    
    
}
