//
//  SceneDelegate.swift
//  RicTy
//
//  Created by AliReza on 2022-11-24.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit


class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    var appCoordinator: AppCoordinatorProtocol?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        handleUserState(windowScene: windowScene)
        
    }
    
    func handleUserState(windowScene: UIWindowScene) {

        let dispatchGroup = DispatchGroup()

        dispatchGroup.enter()
        UserDefaultsManager.shared.googleSignIn {
            dispatchGroup.leave()
        }

        dispatchGroup.enter()
        UserDefaultsManager.shared.facebookLogin {
            dispatchGroup.leave()
        }

        dispatchGroup.notify(queue: .main) { [weak self] in
            self?.startApplication(windowScene: windowScene)
        }
    }
    
    func startApplication(windowScene: UIWindowScene) {
        
        let navigationController = RTNavigationController()
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        appCoordinator = AppCoordinator(type: .app, navigationController: navigationController)
        appCoordinator?.start()
    }

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else { return }
        
        ApplicationDelegate.shared.application(UIApplication.shared, open: url, sourceApplication: nil, annotation: [UIApplication.OpenURLOptionsKey.annotation])
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        UserDefaultsManager.shared.updateDataBase()
    }


}

