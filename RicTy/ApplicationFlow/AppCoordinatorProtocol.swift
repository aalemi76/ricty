//
//  AppCoordinatorProtocol.swift
//  RicTy
//
//  Created by AliReza on 2022-11-27.
//

import Foundation

protocol AppCoordinatorProtocol: Coordinator {
    
    func showOnBoardingFlow()
    
    func showLoginFlow()
    
    func showMainFlow()
    
}
