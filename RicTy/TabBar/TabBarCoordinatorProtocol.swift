//
//  TabBarCoordinatorProtocol.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit

protocol TabBarCoordinatorProtocol: Coordinator {
    
    var tabBarController: UITabBarController { get set }
    
}
