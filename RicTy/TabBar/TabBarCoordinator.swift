//
//  TabBarCoordinator.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit
import Combine

enum TabType: CaseIterable {
    case home
    case favorite
    case profile
}

class TabBarCoordinator: TabBarCoordinatorProtocol {
    
    var didFinish: PassthroughSubject<Coordinator, Never>
    
    var type: CoordinatorType
    
    var navigationController: UINavigationController
    
    var tabBarController: UITabBarController
    
    var childCoordinators: [Coordinator]
    
    var cancellableStorage: Set<AnyCancellable>
    
    required init(type: CoordinatorType, navigationController: UINavigationController) {
        didFinish = PassthroughSubject()
        self.type = type
        self.navigationController = navigationController
        tabBarController = UITabBarController()
        childCoordinators = []
        cancellableStorage = []
        self.navigationController.setNavigationBarHidden(true, animated: false)
    }
    
    func start() {
        let tabs = TabType.allCases
        tabs.forEach({ createTab(with: $0) })
        createTabBar(childCoordinators.map({ $0.navigationController }))
    }
    
    private func createTab(with type: TabType) {
        
        let navigationController = RTNavigationController()
        
        switch type {
            
        case .home:
            navigationController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(systemName: "house"), selectedImage: UIImage(systemName: "house.fill"))
            let coordinator = HomeCoordinator(type: .tab, navigationController: navigationController)
            childCoordinators.append(coordinator)
            coordinator.start()
        case .favorite:
            navigationController.tabBarItem = UITabBarItem(title: "Favorite", image: UIImage(systemName: "star"), selectedImage: UIImage(systemName: "star.fill"))
            let coordinator = FavoriteCoordinator(type: .tab, navigationController: navigationController)
            childCoordinators.append(coordinator)
            coordinator.start()
        case .profile:
            navigationController.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(systemName: "person"), selectedImage: UIImage(systemName: "person.fill"))
            let coordinator = ProfileCoordinator(type: .tab, navigationController: navigationController)
            coordinator.didFinish.sink { [weak self] _ in
                self?.end()
            }.store(in: &cancellableStorage)
            childCoordinators.append(coordinator)
            coordinator.start()
        }
        
    }
    
    private func createTabBar(_ viewControllers: [UIViewController]) {
        
        tabBarController.setViewControllers(viewControllers, animated: true)
        tabBarController.selectedIndex = 0
        tabBarController.view.backgroundColor = GlobalSettings.shared().black
        
        if #available(iOS 15, *) {
            
            let appearance = UITabBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = GlobalSettings.shared().mainColor
            tabBarController.tabBar.standardAppearance = appearance
            tabBarController.tabBar.scrollEdgeAppearance = tabBarController.tabBar.standardAppearance
            
        } else {
            tabBarController.tabBar.isTranslucent = false
            tabBarController.tabBar.backgroundColor = GlobalSettings.shared().mainColor
        }
        tabBarController.tabBar.tintColor = GlobalSettings.shared().black
        tabBarController.tabBar.unselectedItemTintColor = .white
        tabBarController.tabBar.layer.masksToBounds = true
        tabBarController.tabBar.layer.cornerRadius = 20
        tabBarController.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        navigationController.viewControllers = [tabBarController]
    }
}
