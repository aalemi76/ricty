//
//  OnBoardingCoordinator.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit
import Combine

class OnBoardingCoordinator: Coordinator {
    var didFinish: PassthroughSubject<Coordinator, Never>
    
    var type: CoordinatorType
    
    var navigationController: UINavigationController
    
    var childCoordinators: [Coordinator]
    
    var cancellableStorage: Set<AnyCancellable>
    
    required init(type: CoordinatorType, navigationController: UINavigationController) {
        didFinish = PassthroughSubject()
        self.type = type
        self.navigationController = navigationController
        childCoordinators = []
        cancellableStorage = []
    }
    
    func start() {
        let viewModel = OnBoardingViewModel()
        sink(with: viewModel)
        let viewController = OnBoardingViewController(viewModel: viewModel)
        viewController.title = "Welcome"
        navigationController.setNavigationBarHidden(true, animated: false)
        push(viewController: viewController, animated: true)
    }
    
    func sink(with viewModel: OnBoardingViewModelProtocol) {
        viewModel.didTapOnGetStarted.sink { [unowned self] _ in
            self.didFinish.send(self)
        }.store(in: &cancellableStorage)
    }
    
}
