//
//  OnBoardingViewModel.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import Foundation
import Combine

class OnBoardingViewModel: OnBoardingViewModelProtocol {
    
    var didTapOnGetStarted =  PassthroughSubject<Any, Never>()
    
    weak var view: Viewable?
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
    }
    
    func didTapOnGetStartedButton() {
        didTapOnGetStarted.send(true)
    }
    
}
