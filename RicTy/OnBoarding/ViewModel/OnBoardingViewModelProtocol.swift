//
//  OnBoardingViewModelProtocol.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import Foundation
import Combine

protocol OnBoardingViewModelProtocol: ViewModelProvider {
    
    var didTapOnGetStarted: PassthroughSubject<Any, Never> { get set }
    
    func didTapOnGetStartedButton()
    
}
