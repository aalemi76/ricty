//
//  OnBoardingViewController.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit

class OnBoardingViewController: SharedViewController {
    
    let viewModel: OnBoardingViewModelProtocol
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "applicationLogo"))
        view.contentMode = .scaleAspectFit
        view.backgroundColor = .none
        return view
    }()
    
    private lazy var pulseLayers = [CAShapeLayer]()
    
    private lazy var descriptionLabel = UILabel()
    
    init(viewModel: OnBoardingViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func didTapOnButton() {
        viewModel.didTapOnGetStartedButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad(self)
        setupSubViews()
    }
    
    func setupSubViews() {
        view.backgroundColor = GlobalSettings.shared().black
        addTitleLabel()
        addImageView()
        addDescriptionLabel()
        addGetStartedButton()
    }
    
    func addTitleLabel() {
        let label = UILabel()
        label.text = "RicTy"
        label.font = UIFont(name: "Chalkduster", size: 65)
        label.textColor = GlobalSettings.shared().darkGreen
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10)
        ])
    }
    
    func addDescriptionLabel() {
        let text: NSMutableAttributedString = NSMutableAttributedString(string: "The RicTy is an application based on the television show Rick and Morty. You will have access to about hundreds of characters, images, locations and episodes. The RicTy app is filled with canonical information as seen on the TV show. Let's jump in.")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .justified
        paragraphStyle.baseWritingDirection = .leftToRight
        paragraphStyle.lineBreakMode = .byWordWrapping
        text.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, text.length))
        descriptionLabel.numberOfLines = 0
        descriptionLabel.font = GlobalSettings.shared().systemFont(type: .medium, size: 15)
        descriptionLabel.textColor = GlobalSettings.shared().lightGreen
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.attributedText = text
        view.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            descriptionLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            descriptionLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
    }
    
    func addGetStartedButton() {
        let button = UIButton()
        button.setTitle("Let's get started", for: .normal)
        button.backgroundColor = GlobalSettings.shared().mainColor
        button.setTitleColor(GlobalSettings.shared().black, for: .normal)
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.heightAnchor.constraint(equalToConstant: 50),
            button.widthAnchor.constraint(equalToConstant: 150),
            button.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 30)])
        button.addTarget(self, action: #selector(didTapOnButton), for: .touchUpInside)
    }
    
    func addImageView() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 100),
            imageView.heightAnchor.constraint(equalToConstant: 100),
            imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 100)])
        for _ in 0...7 {
            addPulsePath()
        }
        for pulseLayer in pulseLayers {
            animatePulseLayer(pulseLayer)
        }
    }
    
    func addPulsePath() {
        let radius: CGFloat = 50
        let path = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        let pulseLayer = CAShapeLayer()
        pulseLayer.path = path.cgPath
        pulseLayer.strokeColor = GlobalSettings.shared().mainColor?.cgColor
        pulseLayer.lineWidth = 0.8
        pulseLayer.opacity = 1
        pulseLayer.fillColor = .none
        pulseLayer.lineCap = CAShapeLayerLineCap.round
        pulseLayer.position = CGPoint(x: radius, y: radius)
        pulseLayers.append(pulseLayer)
        imageView.layer.insertSublayer(pulseLayer, below: imageView.layer)
        
    }
    
    func animatePulseLayer(_ pulseLayer: CAShapeLayer) {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = Double.random(in: 100...200)/100
        animation.duration = 1
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = .infinity
        pulseLayer.add(animation, forKey: "pulsing")
    }
}
