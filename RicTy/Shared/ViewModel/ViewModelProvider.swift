//
//  ViewModelProvider.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import Foundation

protocol ViewModelProvider: AnyObject {
    
    func viewDidLoad(_ view: Viewable)
    
}

