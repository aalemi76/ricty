//
//  ViewModelProviderMappable.swift
//  RicTy
//
//  Created by AliReza on 2022-11-30.
//

import Foundation

protocol ViewModelProviderMappable: ViewModelProvider {
    
    init(interactor: Interactor)
    
}
