//
//  TableViewDataSourceHandler.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import UIKit
import Combine

class TableViewDataSourceHandler: NSObject, UITableViewDataSource, UITableViewDataSourcePrefetching {
    var sections: [Sectionable]
    var loadNextPage = PassthroughSubject<IndexPath, Never>()
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].getCells().count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = sections[indexPath.section].getCells()[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.getReuseID(), for: indexPath)
        cell.selectionStyle = .none
        (cell as? Updatable)?.attach(viewModel: item)
        (cell as? Updatable)?.update(model: item.getModel())
        return cell
    }
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        guard let indexPath = indexPaths.last, indexPath.section == sections.count - 1 else { return }
        let section = sections[indexPath.section]
        guard indexPath.row < section.getCells().count && indexPath.row == section.getCells().count - 1 else { return }
        loadNextPage.send(indexPath)

    }
}
