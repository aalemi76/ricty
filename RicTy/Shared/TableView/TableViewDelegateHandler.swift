//
//  TableViewDelegateHandler.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import UIKit
import Combine

class TableViewDelegateHandler: NSObject, UITableViewDelegate {
    var sections: [Sectionable]
    var passSelectedItem = PassthroughSubject<Any, Never>()
    var passSelectedSection = PassthroughSubject<Int, Never>()
    var scrollViewDidScroll = PassthroughSubject<UIScrollView, Never>()
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = sections[section].getHeaderView()
        return header as? UIView
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = sections[section].getFooterView()
        return footer as? UIView
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.textLabel?.textColor = GlobalSettings.shared().white
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = sections[indexPath.section].getCells()[indexPath.row].getModel()
        passSelectedItem.send(item)
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        setupContexMenu(indexPath: indexPath)
    }
    
    func setupContexMenu(indexPath: IndexPath) -> UIContextMenuConfiguration? {
        guard let cellViewModel = sections[indexPath.section].getCells()[indexPath.row] as? ContextMenuProvider,
              let entity = cellViewModel.didForceTouchOnCell() else { return nil}
        
        let actionTitle = entity.isFavorite ? "Unfavorite" : "Favorite"
        let actionImage = entity.isFavorite ? "star.fill" : "star"
        
        let contextMenuConfiguration = UIContextMenuConfiguration(actionProvider:  { _ -> UIMenu? in
            
            let favoriteAction = UIAction(title: actionTitle, image: UIImage(systemName: actionImage)) { action in
                cellViewModel.didTapOnFavoriteOption()
            }
            
            let shareAction = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { action in
                cellViewModel.didTapOnShareOption.send(entity)
            }
            
            return UIMenu(options: .displayInline, children: [favoriteAction, shareAction])
        })
        return contextMenuConfiguration
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollViewDidScroll.send(scrollView)
    }
}
