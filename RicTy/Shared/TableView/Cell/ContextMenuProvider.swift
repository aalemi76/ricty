//
//  ContextMenuProvider.swift
//  RicTy
//
//  Created by AliReza on 2022-12-08.
//

import UIKit
import Combine

protocol ContextMenuProvider {
    var didTapOnShareOption: PassthroughSubject<SharedEntity, Never> { get }
    func didForceTouchOnCell() -> SharedEntity?
    func didTapOnFavoriteOption()
}
