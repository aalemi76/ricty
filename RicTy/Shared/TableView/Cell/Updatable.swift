//
//  Updatable.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import Foundation

protocol Updatable: AnyObject {
    
    func attach(viewModel: Reusable)
    
    func update(model: Any)
}
