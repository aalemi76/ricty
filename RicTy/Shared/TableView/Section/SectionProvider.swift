//
//  SectionProvider.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import UIKit

class SectionProvider: Sectionable {
    
    var title: String?
    private var cells: [Reusable]
    private var headerView: ReusableView?
    private var footerView: ReusableView?
    
    required init(title: String?, cells: [Reusable], headerView: ReusableView?, footerView: ReusableView?) {
        self.title = title
        self.cells = cells
        self.headerView = headerView
        self.footerView = footerView
    }
    
    func getCells() -> [Reusable] { return cells }
    func getHeaderView() -> ReusableView? { return headerView }
    func getFooterView() -> ReusableView? { return footerView }
    
    func append(_ cells: [Reusable]) {
        self.cells += cells
    }
    
    func removeCells() {
        cells.removeAll()
    }
}
