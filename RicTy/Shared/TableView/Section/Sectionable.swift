//
//  Sectionable.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import UIKit

protocol Sectionable: AnyObject {
    var title: String? { get }
    init(title: String?, cells: [Reusable], headerView: ReusableView?, footerView: ReusableView?)
    func getCells() -> [Reusable]
    func getHeaderView() -> ReusableView?
    func getFooterView() -> ReusableView?
    func append(_ cells: [Reusable])
    func removeCells()
}
