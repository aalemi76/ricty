//
//  Routes.swift
//  RicTy
//
//  Created by AliReza on 2022-12-01.
//

import Foundation

enum Routes: String {
    
    case baseURL = "https://rickandmortyapi.com/api/"
    case characters = "character"
    case episodes = "episode"
    case locations = "location"
    
    func generateURL() -> String {
        switch self {
        case .baseURL:
            return rawValue
        default:
            return Routes.baseURL.rawValue + rawValue
        }
    }
    
    func generateURL(withIDs ids: String) -> String {
        switch self {
        case .characters:
            return Routes.baseURL.rawValue + rawValue + "/" + "\(ids)"
        case .episodes:
            return Routes.baseURL.rawValue + rawValue + "/" + "\(ids)"
        case .locations:
            return Routes.baseURL.rawValue + rawValue + "/" + "\(ids)"
        default:
            return rawValue
        }
    }
}
