//
//  InteractorProvider.swift
//  RicTy
//
//  Created by AliReza on 2022-12-04.
//

import Foundation
import RealmSwift
import Alamofire

protocol InteractorProvider {
    
    func getModel<Model: Codable>(_ route: Routes, method: HTTPMethod, parameters: [String: Any]?, encoding: URLEncoding, headers: HTTPHeaders?, onSuccess: @escaping (Model) -> Void, onFailure: @escaping (RTError) -> Void)
    
    func getModel<Model: StorableObject>(withPredicate predicate: NSPredicate?, sortedByKey sortKey: String?, inAscending isAscending: Bool) -> Results<Model>
    
    func updateFetchedObject<Model: StorableObject>(_ model: Model, withPredicate predicate: NSPredicate?, sortedByKey sortKey: String?, inAscending isAscending: Bool)
    
    func updateDBObject<Model: StorableObject>(_ model: Model, withPredicate predicate: NSPredicate?, sortedByKey sortKey: String?, inAscending isAscending: Bool, block: @escaping ((Model?) -> Void))
    
    func updateDB(ofType type: ListType)
    
    // add single object to DB
    func addEntity(_ entity: StorableObject)
}
