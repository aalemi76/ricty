//
//  Interactor.swift
//  RicTy
//
//  Created by AliReza on 2022-11-30.
//

import Foundation
import RealmSwift
import Alamofire

class Interactor: InteractorProvider {
    
    func getModel<Model: Codable>(_ route: Routes, method: HTTPMethod = .get, parameters: [String: Any]? = nil, encoding: URLEncoding = .queryString, headers: HTTPHeaders? = nil, onSuccess: @escaping (Model) -> Void, onFailure: @escaping (RTError) -> Void) {
        
        guard NetworkReachabilityManager()?.isReachable ?? true else {
            onFailure(.internetConnectionError)
            return
        }
        var url = ""
        if let ids = parameters?["ids"] as? String {
            url = route.generateURL(withIDs: ids)
        } else {
            url = route.generateURL()
        }
        AF.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseDecodable(of: Model.self, queue: .global(qos: .background)) { response in
            if response.response?.statusCode == 404 {
                onFailure(.couldNotFindObject)
                return
            }
            guard let object = response.value else {
                onFailure(.inconsistentData)
                return
            }
            
            onSuccess(object)
        }
    }
    
    func getModel<Model: StorableObject>(withPredicate predicate: NSPredicate? = nil, sortedByKey sortKey: String? = nil, inAscending isAscending: Bool = true) -> Results<Model> {
        return DBManager.shared.get(from: Model.self, withPredicate: predicate, sortedByKey: sortKey, inAscending: isAscending)
    }
    
    func updateFetchedObject<Model: StorableObject>(_ model: Model, withPredicate predicate: NSPredicate? = nil, sortedByKey sortKey: String? = nil, inAscending isAscending: Bool = true) {
        guard let dbEntity: Model = getModel(withPredicate: predicate, sortedByKey: sortKey, inAscending: isAscending).first else { return }
        model.updateObject(withEntity: dbEntity)
    }
    
    func updateDBObject<Model: StorableObject>(_ model: Model, withPredicate predicate: NSPredicate? = nil, sortedByKey sortKey: String? = nil, inAscending isAscending: Bool = true, block: @escaping ((Model?) -> Void)) {
        
        guard let dbEntity: Model = getModel(withPredicate: predicate, sortedByKey: sortKey, inAscending: isAscending).first else {
            block(nil)
            return
        }
        DBManager.shared.update(dbEntity) { dbEntity in
            block(dbEntity)
        }
    }
    
    func updateDB(ofType type: ListType) {
        switch type {
        case .characters:
            let characters = DBManager.shared.get(from: Character.self, withPredicate: NSPredicate(format: "isFavorite == %i", 0), sortedByKey: nil, inAscending: true)
            DBManager.shared.deleteEntities(characters)
        case .episodes:
            let episodes = DBManager.shared.get(from: Episode.self, withPredicate: NSPredicate(format: "isFavorite == %i", 0), sortedByKey: nil, inAscending: true)
            DBManager.shared.deleteEntities(episodes)
        case .locations:
            let locations = DBManager.shared.get(from: Location.self, withPredicate: NSPredicate(format: "isFavorite == %i", 0), sortedByKey: nil, inAscending: true)
            DBManager.shared.deleteEntities(locations)
        }
    }
    
    func addEntity(_ entity: StorableObject) {
        DBManager.shared.addEntity(entity)
    }
}
