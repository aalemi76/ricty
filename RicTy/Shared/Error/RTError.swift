//
//  RTError.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import Foundation

enum RTError: String, Error {
    case successfulLogin = "Login successfully!"
    case unsuccessfulLogin = "Cannot login the user!"
    case cannotRetrieveUser = "An error occurred while retrieving the user information."
    case cannotSaveUser = "Unable to save user information."
    case inconsistentData = "The data is inconsistent with expected type."
    case couldNotFindObject = "Sorry, we couldn't find the object."
    case internetConnectionError = "The Internet connection appears to be offline."
}
