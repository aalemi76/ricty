//
//  CollectionViewDelegateHandler.swift
//  RicTy
//
//  Created by AliReza on 2022-12-06.
//

import UIKit
import Combine

class CollectionViewDelegateHandler: NSObject, UICollectionViewDelegate {
    var sections: [Sectionable]
    
    private var passSelectedItem = PassthroughSubject<Any, Never>()
    
    func getSelectedItem() -> PassthroughSubject<Any, Never> { return passSelectedItem }
    
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = sections[indexPath.section].getCells()[indexPath.row].getModel()
        passSelectedItem.send(item)
    }
    
}

