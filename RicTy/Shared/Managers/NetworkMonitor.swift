//
//  NetworkMonitor.swift
//  RicTy
//
//  Created by AliReza on 2022-12-03.
//

import Network
import Combine

class NetworkMonitor {
    
    var didConnectedToInternet = PassthroughSubject<Bool, Never>()
    
    let monitor = NWPathMonitor()
    
    func startMonitoring() {
        
        monitor.pathUpdateHandler = { [weak self] path in
            self?.didConnectedToInternet.send(path.status == .satisfied)
        }
        
        let queue = DispatchQueue(label: "NetworkMonitor")
        monitor.start(queue: queue)
        
    }
    
    func stopMonitoring() {
        monitor.cancel()
    }
}
