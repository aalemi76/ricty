//
//  UserDefaultsManager.swift
//  RicTy
//
//  Created by AliReza on 2022-11-27.
//

import Foundation
import GoogleSignIn
import FBSDKLoginKit

final class UserDefaultsManager {
    
    static let shared: UserDefaultsManager = {
        return UserDefaultsManager()
    }()
    
    private let interactor = Interactor()
    
    private let appFirstLaunch = "isFirstLaunch"
    
    var isSignedIn = false
    
    func getUser() -> User? {
        return interactor.getModel().first
    }
    
    func updateUser(_ user: User) {
        interactor.updateDBObject(user) { [weak self] dbUser in
            if let dbUser = dbUser {
                dbUser.updateObject(withEntity: user)
            } else {
                self?.interactor.addEntity(user)
            }
            self?.isSignedIn = true
        }
    }
    
    func signOut() {
        
        LoginManager().logOut()
        GIDSignIn.sharedInstance.signOut()
        
        if let user = getUser() {
            
            DBManager.shared.deleteEntity(user)
            isSignedIn = false
        }
        
        return
    }
    
    func googleSignIn(onComplete: @escaping () -> Void) {
        GIDSignIn.sharedInstance.restorePreviousSignIn { [weak self] user, error in

            if error != nil || user == nil {
                onComplete()

            } else if let profile = user?.profile {

                let user = User(givenName: profile.givenName ?? "", lastName: profile.familyName ?? "", emailAddress: profile.email, profilePicURL: profile.imageURL(withDimension: 1080)?.absoluteString ?? "")
                self?.updateUser(user)
                onComplete()
            }
        }
    }
    
    func facebookLogin(onComplete: @escaping () -> Void) {
        
        Profile.loadCurrentProfile { [weak self] profile, error in
            
            guard error == nil, let profile = profile else {
                onComplete()
                return
            }
            
            let name = profile.name?.split(separator: " ")
            
            let user = User(givenName: "\(name?.first ?? "")", lastName: "\(name?.last ?? "")", emailAddress: profile.email ?? "", profilePicURL: profile.imageURL(forMode: .large, size: CGSize(width: 1080, height: 1080))?.absoluteString ?? "")
            self?.updateUser(user)
            onComplete()
        }
    }
    
    func updateDataBase() {
        ListType.allCases.forEach({ interactor.updateDB(ofType: $0) })
    }
    
    func isFirstLaunch() -> Bool {
        if let isFirstLaunch = UserDefaults.standard.object(forKey: appFirstLaunch) as? Bool {
            return isFirstLaunch
        } else {
            UserDefaults.standard.set(false, forKey: appFirstLaunch)
            return true
        }
    }
    
}
