//
//  DBManager.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import Foundation
import RealmSwift

final class DBManager: DBManagerProvider {
    
    static var shared: DBManagerProvider = DBManager()
    
    var realmInstance: RealmSwift.Realm
    
    var threadName: String = "realm"
    
    init() {
        do {
            realmInstance = try Realm(configuration: DBManager.makeRealmConfig())
        } catch {
            print(error)
            fatalError("Unable to create an instance of Realm")
        }
    }
    
    func write<Model>(_ entity: Model? = nil, block: @escaping ((RealmSwift.Realm, Model?) -> Void)) where Model : StorableObject {
        
        DispatchQueue(label: threadName).sync {
            
            autoreleasepool {
                
                guard !realmInstance.isInWriteTransaction else { return }
                
                do {
                    try realmInstance.write {
                        block(realmInstance, entity)
                    }
                } catch {
                    print(error)
                    return
                }
            }
        }
        
    }
    
    func addEntity(_ entity: StorableObject) {
        
        write { instance, _ in
            instance.add(entity, update: .all)
        }
        
    }
    
    func addEntities<List>(_ entities: List) where List : Sequence, List.Element : StorableObject {
        
        write { instance, _ in
            instance.add(entities, update: .all)
        }
        
    }
    
    func get<Model>(from type: Model.Type, withPredicate predicate: NSPredicate?, sortedByKey sortKey: String?, inAscending isAscending: Bool) -> RealmSwift.Results<Model> where Model : StorableObject {
        
        var objects = realmInstance.objects(type)
        
        if let predicate = predicate {
            objects = objects.filter(predicate)
        }
        
        if let sortKey = sortKey {
            objects = objects.sorted(byKeyPath: sortKey, ascending: isAscending)
        }
        
        return objects
        
    }
    
    func deleteEntity(_ entity: StorableObject) {
        
        write(entity) { instance, newEntity in
            guard let newEntity = newEntity, !newEntity.isInvalidated else { return }
            instance.delete(newEntity)
        }
        
    }
    
    func deleteEntities<List>(_ entities: List) where List : Sequence, List.Element : StorableObject {
        
        write { instance, _ in
            instance.delete(entities)
        }
    }
    
    func delete(form type: StorableObject.Type, withPredicate predicate: NSPredicate?) {
        
        deleteEntities(get(from: type, withPredicate: predicate, sortedByKey: nil, inAscending: true))
        
    }
    
    func update<Model>(_ entity: Model, block: @escaping ((Model) -> Void)) where Model : StorableObject {
        
        guard !entity.isInvalidated else { return }
        
        write(entity) { _, newEntity in
            guard let newEntity = newEntity, !newEntity.isInvalidated else { return }
            block(newEntity)
        }
        
    }
    
    private static func makeRealmConfig() -> RealmSwift.Realm.Configuration {
        
        return Realm.Configuration(schemaVersion: 2, migrationBlock: { (migration, oldSchemaVersion) in
            
            // Migration block. Use it when you upgrade the schema version.
            
        })
    }
    
}
