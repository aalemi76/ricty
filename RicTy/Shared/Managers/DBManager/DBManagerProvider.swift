//
//  DBManagerProvider.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import Foundation
import RealmSwift

protocol DBManagerProvider {
    
    // Shared instance
    static var shared: DBManagerProvider { get }
    
    var realmInstance: Realm { get }
    
    var threadName: String { get }
    
    // Write operation
    func write<Model: StorableObject>(_ entity: Model?, block: @escaping ((Realm, Model?) -> Void))
    
    // add single object to DB
    func addEntity(_ entity: StorableObject)
    
    // add list of objects to DB
    func addEntities<List: Sequence>(_ entities: List) where List.Iterator.Element: StorableObject
    
    // get objects from DB that satisfy the given predicate
    func get<Model: StorableObject>(from type: Model.Type, withPredicate predicate: NSPredicate?, sortedByKey sortKey: String?, inAscending isAscending: Bool) -> Results<Model>
    
    // delete single object from DB
    func deleteEntity(_ entity: StorableObject)
    
    // delete list of objects from DB
    func deleteEntities<List: Sequence>(_ entities: List) where List.Iterator.Element: StorableObject
    
    // delete an entity from DB based on the given predicate
    func delete(form type: StorableObject.Type, withPredicate predicate: NSPredicate?)
    
    // update and overwrite an entity inside DB
    func update<Model: StorableObject>(_ entity: Model, block: @escaping ((Model) -> Void))
    
}
