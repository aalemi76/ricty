//
//  Pageable.swift
//  RicTy
//
//  Created by AliReza on 2022-12-02.
//

import Foundation

protocol Pageable: Viewable {
    
    func showNexPage(_ result: Result<Any, RTError>)
    
}
