//
//  ReusableView.swift
//  RicTy
//
//  Created by AliReza on 2022-12-06.
//

import Foundation

protocol ReusableView {
    func getReuseID() -> String
    func getViewClass() -> AnyClass
}
