//
//  Viewable.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import Foundation

protocol Viewable: AnyObject {
    
    func show(result: Result<Any, RTError>)
    
    
    
}
