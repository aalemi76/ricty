//
//  RTNavigationController.swift
//  RicTy
//
//  Created by AliReza on 2022-11-27.
//

import UIKit

class RTNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = GlobalSettings.shared().black
        appearance.titleTextAttributes = [.foregroundColor: GlobalSettings.shared().mainColor ?? .white]
        appearance.largeTitleTextAttributes = [.foregroundColor: GlobalSettings.shared().mainColor ?? .white]
        navigationBar.tintColor = .white
        navigationBar.standardAppearance = appearance
        navigationBar.compactAppearance = appearance
        navigationBar.scrollEdgeAppearance = appearance
    }
    
}
