//
//  FavoriteCoordinator.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit
import Combine

class FavoriteCoordinator: Coordinator {
    
    var didFinish: PassthroughSubject<Coordinator, Never>
    
    var type: CoordinatorType
    
    var navigationController: UINavigationController
    
    var childCoordinators: [Coordinator]
    
    var cancellableStorage: Set<AnyCancellable>
    
    required init(type: CoordinatorType, navigationController: UINavigationController) {
        didFinish = PassthroughSubject()
        self.type = type
        self.navigationController = navigationController
        childCoordinators = []
        cancellableStorage = []
    }
    
    func start() {
        let viewModel = FavoriteViewModel()
        sinkWithViewModel(viewModel)
        let viewController = FavoriteViewController(viewModel: viewModel)
        viewController.title = "Favorite"
        navigationController.setNavigationBarHidden(false, animated: false)
        push(viewController: viewController, animated: true)
    }
    
    func sinkWithViewModel(_ viewModel: FavoriteViewModel) {
        viewModel.showFavoriteList.sink { [weak self] listType in
            self?.showFavoriteList(for: listType)
        }.store(in: &cancellableStorage)
    }
    
    private func showFavoriteList(for listType: ListType) {
        switch listType {
            
        case .characters:
            let viewModel = FavoriteListViewModel<Character>(type: listType, interactor: Interactor())
            showUIActivityViewController(viewModel: viewModel)
            let viewController = ListViewController(viewModel: viewModel)
            push(viewController: viewController, animated: true)
        case .episodes:
            let viewModel = FavoriteListViewModel<Episode>(type: listType, interactor: Interactor())
            showUIActivityViewController(viewModel: viewModel)
            let viewController = ListViewController(viewModel: viewModel)
            push(viewController: viewController, animated: true)
        case .locations:
            let viewModel = FavoriteListViewModel<Location>(type: listType, interactor: Interactor())
            showUIActivityViewController(viewModel: viewModel)
            let viewController = ListViewController(viewModel: viewModel)
            push(viewController: viewController, animated: true)
        }
    }
    
    func showDetailViewController(id: Int) {
        return
    }
    
    func showUIActivityViewController(viewModel: some ListViewModelProvider) {
        viewModel.showUIActivityViewController.sink { [weak self] shareItems in
            let viewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            self?.present(viewController: viewController, animated: true)
        }.store(in: &cancellableStorage)
    }
    
}
