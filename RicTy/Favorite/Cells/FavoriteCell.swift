//
//  FavoriteCell.swift
//  RicTy
//
//  Created by AliReza on 2022-12-09.
//

import UIKit

class FavoriteCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: FavoriteCell.self)
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        guard let model = model as? ListType else { return }
        titleLabel.text = model.rawValue.capitalized
        cellImageView.image = UIImage(named: model.getHeaderViewName())
    }
    
}
