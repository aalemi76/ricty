//
//  FavoriteViewController.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit

class FavoriteViewController: SharedViewController {
    
    let viewModel: FavoriteViewModel
    
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .insetGrouped)
        tbl.backgroundColor = .none
        tbl.tableHeaderView = nil
        tbl.tableFooterView = nil
        tbl.sectionHeaderHeight = 0
        tbl.sectionFooterHeight = 20
        tbl.separatorStyle = .none
        tbl.rowHeight = 66
        return tbl
    }()
    
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        return container
    }()
    
    init(viewModel: FavoriteViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTableView()
        viewModel.viewDidLoad(self)
        showLoadingView()
    }
    
    func addTableView() {
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }
    
}
