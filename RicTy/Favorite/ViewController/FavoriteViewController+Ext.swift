//
//  FavoriteViewController+Ext.swift
//  RicTy
//
//  Created by AliReza on 2022-12-09.
//

import Foundation

extension FavoriteViewController: Viewable {
    func show(result: Result<Any, RTError>) {
        
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let data):
                
                guard let sections = data as? [Sectionable] else {
                    self?.showErrorBanner(title: "An error occured while loading posts.")
                    return
                }
                self?.configureTableView(sections)
                
            case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
                self?.dismissLoadingView()
            }
            
        }
    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
        sinkWithTapOnCells()
        dismissLoadingView()
    }
    
    func sinkWithTapOnCells() {
        tableViewContainer.delegateHandler.passSelectedItem.sink { [weak self] model in
            self?.viewModel.didTapOnCell(withModel: model)
        }.store(in: &cancellableStorage)
    }
}
