//
//  FavoriteListViewModel.swift
//  RicTy
//
//  Created by AliReza on 2022-12-10.
//

import Foundation
import RealmSwift
import Combine

class FavoriteListViewModel<Model: SharedEntity>: ListViewModelProvider {
    
    var showUIActivityViewController = PassthroughSubject<[Any], Never>()
    
    private let interactor: Interactor
    
    var type: ListType
    
    weak var view: Viewable?
    
    var isFetchInProgress = false
    
    var cancellableStorage = Set<AnyCancellable>()
    
    required init(type: ListType, interactor: Interactor) {
        self.interactor = interactor
        self.type = type
    }
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        getDataFromDB()
    }
    
    private func getDataFromDB() {
        guard !isFetchInProgress else { return }
        isFetchInProgress = true
        let idsString = getFavoriteEntitiesID()
        interactor.getModel(type.getRoute(), parameters: ["ids": idsString]) { [unowned self] (object: [Model]) in
            var cells = [Reusable]()
            switch type {
            case .characters:
                cells = object.map({CharacterCellViewModel(reuseID: type.getCellReuseID(), cellClass: type.getCellClass(), model: $0)})
            case .episodes:
                cells = object.map({ EpisodeCellViewModel(reuseID: type.getCellReuseID(), cellClass: type.getCellClass(), model: $0)})
            case .locations:
                cells = object.map({ LocationCellViewModel(reuseID: type.getCellReuseID(), cellClass: type.getCellClass(), model: $0)})
            }
            cells.forEach({ sinkWithTapOnShareOption($0 as? ContextMenuProvider)} )
            let newSections = cells.map({ SectionProvider(title: nil, cells: [$0], headerView: nil, footerView: nil)})
            view?.show(result: .success(newSections))
            isFetchInProgress = false
        } onFailure: { [weak self] error in
            self?.view?.show(result: .failure(error))
            self?.isFetchInProgress = false
        }
    }
    
    private func getFavoriteEntitiesID() -> String {
        switch type {
        case .characters:
            let ids: Results<Character> = interactor.getModel()
            return makeString(fromIDs: ids.map({ $0.identifier }))
        case .episodes:
            let ids: Results<Episode> = interactor.getModel()
            return makeString(fromIDs: ids.map({ $0.identifier }))
        case .locations:
            let ids: Results<Location> = interactor.getModel()
            return makeString(fromIDs: ids.map({ $0.identifier }))
        }
    }
    
    private func makeString(fromIDs ids: [Int?]) -> String {
        var idString = ""
        for id in ids {
            if let id = id {
                idString += "\(id),"
            }
        }
        return String(idString.dropLast())
    }
    
    func sinkWithTapOnShareOption(_ cellViewModel: ContextMenuProvider?) {
        cellViewModel?.didTapOnShareOption.sink(receiveValue: { [unowned self] model in
            
            var shareItems = [Any]()
            
            switch type {
                
            case .characters:
                guard let model = model as? Character else { return }
                shareItems = [model.name ?? "", model.status ?? ""]
                if let url = URL(string: model.image ?? "") {
                    shareItems.append(url)
                }
                
            case .episodes:
                guard let model = model as? Episode else { return }
                shareItems = [model.name ?? "", model.episode ?? "", model.airDate ?? ""]
                
            case .locations:
                guard let model = model as? Location else { return }
                shareItems = [model.name ?? "", model.type ?? "", model.dimension ?? ""]
            }
            showUIActivityViewController.send(shareItems)
        }).store(in: &cancellableStorage)
    }
    
    func loadNexPage(from section: Int) {
        return
    }
    
    
}
