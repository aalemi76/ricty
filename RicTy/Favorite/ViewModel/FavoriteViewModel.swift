//
//  FavoriteViewModel.swift
//  RicTy
//
//  Created by AliReza on 2022-12-09.
//

import Foundation
import Combine

class FavoriteViewModel: ViewModelProvider {
    
    var showFavoriteList = PassthroughSubject<ListType, Never>()
    
    weak var view: Viewable?
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        setupSections()
    }
    
    private func setupSections() {
        
        let characterCell = SharedCellViewModel(reuseID: FavoriteCell.reuseID, cellClass: FavoriteCell.self, model: ListType.characters)
        let episodeCell = SharedCellViewModel(reuseID: FavoriteCell.reuseID, cellClass: FavoriteCell.self, model: ListType.episodes)
        let locationCell = SharedCellViewModel(reuseID: FavoriteCell.reuseID, cellClass: FavoriteCell.self, model: ListType.locations)
        
        let cells = [characterCell, episodeCell, locationCell]
        
        let sections = cells.map({ SectionProvider(title: nil, cells: [$0], headerView: nil, footerView: nil) })
        view?.show(result: .success(sections))
        
    }
    
    func didTapOnCell(withModel model: Any) {
        guard let listType = model as? ListType else { return }
        showFavoriteList.send(listType)
    }
}
