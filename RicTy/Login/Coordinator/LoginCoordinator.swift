//
//  LoginCoordinator.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit
import Combine

class LoginCoordinator: Coordinator {
    var didFinish: PassthroughSubject<Coordinator, Never>
    
    var type: CoordinatorType
    
    var navigationController: UINavigationController
    
    var childCoordinators: [Coordinator]
    
    var cancellableStorage: Set<AnyCancellable>
    
    required init(type: CoordinatorType, navigationController: UINavigationController) {
        didFinish = PassthroughSubject()
        self.type = type
        self.navigationController = navigationController
        childCoordinators = []
        cancellableStorage = []
    }
    
    func start() {
        let viewModel = LoginViewModel()
        viewModel.didFinishLoginFlow.sink { [unowned self] _ in
            self.didFinish.send(self)
        }.store(in: &cancellableStorage)
        let viewController = LoginViewController(viewModel: viewModel)
        viewController.title = "Login"
        navigationController.setNavigationBarHidden(true, animated: false)
        push(viewController: viewController, animated: true)
    }
    
    
}
