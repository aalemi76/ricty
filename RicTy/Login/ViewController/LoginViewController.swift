//
//  LoginViewController.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit

class LoginViewController: SharedViewController {
    
    let viewModel: LoginViewModelProtocol
    
    let buttonWidth: CGFloat = 256
    let buttonHeight: CGFloat = 40
    
    lazy var loginProviderStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .center
        view.distribution = .fill
        view.isUserInteractionEnabled = true
        view.autoresizesSubviews = true
        view.spacing = 20
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "applicationLogo"))
        view.contentMode = .scaleAspectFit
        view.backgroundColor = .none
        return view
    }()
    
    private lazy var pulseLayers = [CAShapeLayer]()
    
    init(viewModel: LoginViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func didSelectGoogleSignIn() {
        viewModel.didTapGoogleSignIn()
    }
    
    @objc func didSelectFBLogin() {
        viewModel.didTapFacebookLogin()
    }
    
    @objc func didSelectSkipButton() {
        viewModel.didTapSkipButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad(self)
        setupViewComponents()
    }
    
    func setupViewComponents() {
        
        addTitleLabel()
        addImageView()
        
        view.backgroundColor = GlobalSettings.shared().black
        
        loginProviderStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loginProviderStackView)
        NSLayoutConstraint.activate([
            loginProviderStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loginProviderStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            loginProviderStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            loginProviderStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50)])
        
        addGoogleSignInButton()
        addFBLoginButton()
        addSkipButton()
    }
    
    func addGoogleSignInButton() {
        let button = UIButton()
        button.setImage(UIImage(named: "Google Logo"), for: .normal)
        button.setTitle("  Continue with Google", for: .normal)
        button.backgroundColor = .white
        button.titleLabel?.font = GlobalSettings.shared().systemFont(type: .semiBold, size: 15)
        button.setTitleColor(.darkGray, for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(didSelectGoogleSignIn), for: .touchUpInside)
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: buttonWidth),
            button.heightAnchor.constraint(equalToConstant: buttonHeight)
        ])
        loginProviderStackView.addArrangedSubview(button)
        
    }
    
    func addFBLoginButton() {
        let button = UIButton()
        button.setImage(UIImage(named: "Facebook Logo"), for: .normal)
        button.setTitle("  Continue with Facebook", for: .normal)
        button.backgroundColor = GlobalSettings.shared().blueFB
        button.titleLabel?.font = GlobalSettings.shared().systemFont(type: .semiBold, size: 15)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(didSelectFBLogin), for: .touchUpInside)
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: buttonWidth),
            button.heightAnchor.constraint(equalToConstant: buttonHeight)
        ])
        loginProviderStackView.addArrangedSubview(button)
    }
    
    func addSkipButton() {
        let button = UIButton()
        button.setImage(UIImage(named: "applicationLogoSmall"), for: .normal)
        button.setTitle("  Skip for now", for: .normal)
        button.backgroundColor = GlobalSettings.shared().mainColor
        button.titleLabel?.font = GlobalSettings.shared().systemFont(type: .semiBold, size: 15)
        button.setTitleColor(GlobalSettings.shared().black, for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(didSelectSkipButton), for: .touchUpInside)
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: buttonWidth),
            button.heightAnchor.constraint(equalToConstant: buttonHeight)
        ])
        loginProviderStackView.addArrangedSubview(button)
    }
    
    func addTitleLabel() {
        let label = UILabel()
        label.text = "RicTy"
        label.font = UIFont(name: "Chalkduster", size: 65)
        label.textColor = GlobalSettings.shared().darkGreen
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10)
        ])
    }
    
    func addImageView() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 100),
            imageView.heightAnchor.constraint(equalToConstant: 100),
            imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 100)])
        for _ in 0...7 {
            addPulsePath()
        }
        for pulseLayer in pulseLayers {
            animatePulseLayer(pulseLayer)
        }
    }
    
    func addPulsePath() {
        let radius: CGFloat = 50
        let path = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        let pulseLayer = CAShapeLayer()
        pulseLayer.path = path.cgPath
        pulseLayer.strokeColor = GlobalSettings.shared().mainColor?.cgColor
        pulseLayer.lineWidth = 0.8
        pulseLayer.opacity = 1
        pulseLayer.fillColor = .none
        pulseLayer.lineCap = CAShapeLayerLineCap.round
        pulseLayer.position = CGPoint(x: radius, y: radius)
        pulseLayers.append(pulseLayer)
        imageView.layer.insertSublayer(pulseLayer, below: imageView.layer)
        
    }
    
    func animatePulseLayer(_ pulseLayer: CAShapeLayer) {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = Double.random(in: 100...200)/100
        animation.duration = 1
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = .infinity
        pulseLayer.add(animation, forKey: "pulsing")
    }
}
