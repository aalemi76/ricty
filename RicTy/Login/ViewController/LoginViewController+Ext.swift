//
//  LoginViewController+Ext.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import Foundation

extension LoginViewController: Viewable {
    
    func show(result: Result<Any, RTError>) {
        switch result {
        case .success:
            showSuccessBanner(title: RTError.successfulLogin.rawValue)
        case .failure:
            showErrorBanner(title: RTError.unsuccessfulLogin.rawValue)
        }
    }
    
}
