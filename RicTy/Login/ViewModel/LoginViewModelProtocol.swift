//
//  LoginViewModelProtocol.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import Foundation
import Combine

protocol LoginViewModelProtocol: ViewModelProvider {
    
    var didFinishLoginFlow: PassthroughSubject<Bool, Never> { get set }
    
    func didTapGoogleSignIn()
    
    func didTapFacebookLogin()
    
    func didTapSkipButton()
}
