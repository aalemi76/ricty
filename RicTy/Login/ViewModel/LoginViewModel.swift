//
//  LoginViewModel.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import Foundation
import Combine
import GoogleSignIn
import FBSDKLoginKit

class LoginViewModel: LoginViewModelProtocol {
    
    var didFinishLoginFlow: PassthroughSubject<Bool, Never> = PassthroughSubject()
    
    weak var view: Viewable?
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
    }
    
    func didTapGoogleSignIn() {
        
        guard let viewController = view as? LoginViewController else { return }
        
        let googleSignInConfig = GIDConfiguration(clientID: "813946394884-onrapnsfseri3f60c1es4kcq6f0hmkb8.apps.googleusercontent.com")
        
        GIDSignIn.sharedInstance.signIn(with: googleSignInConfig, presenting: viewController) { [weak self] user, error in
            
            guard error == nil, let profile = user?.profile else {
                self?.view?.show(result: .failure(.cannotRetrieveUser))
                return
            }
            
            let user = User(givenName: profile.givenName ?? "" , lastName: profile.familyName ?? "", emailAddress: profile.email, profilePicURL: profile.imageURL(withDimension: 1080)?.absoluteString ?? "")
            UserDefaultsManager.shared.updateUser(user)
            
            self?.view?.show(result: .success(true))
            self?.didFinishLoginFlow.send(true)

        }
    }
    
    func didTapFacebookLogin() {
        
        guard let viewController = view as? LoginViewController else { return }
        
        let loginManager = LoginManager()
        
        loginManager.logIn(permissions: ["public_profile", "email"], from: viewController) { [weak self] result, error in
            
            guard error == nil, let result = result, !result.isCancelled else {
                self?.view?.show(result: .failure(.cannotRetrieveUser))
                return
            }
            
            self?.loadProfile()
        }
    }
    
    func loadProfile() {

        Profile.loadCurrentProfile { [weak self] profile, error in
            guard let profile = profile else {
                self?.view?.show(result: .failure(.cannotRetrieveUser))
                return
            }
            let name = profile.name?.split(separator: " ")
            
            let user = User(givenName: "\(name?.first ?? "")", lastName: "\(name?.last ?? "")", emailAddress: profile.email ?? "", profilePicURL: profile.imageURL(forMode: .large, size: CGSize(width: 1080, height: 1080))?.absoluteString ?? "")
            UserDefaultsManager.shared.updateUser(user)
            
            self?.view?.show(result: .success(true))
            self?.didFinishLoginFlow.send(true)
            
        }
    }
    
    func didTapSkipButton() {
        didFinishLoginFlow.send(true)
    }
    
}
