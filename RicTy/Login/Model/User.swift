//
//  User.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import Foundation
import RealmSwift

class User: StorableObject {
    
    @Persisted var givenName: String = ""
    @Persisted var lastName: String = ""
    @Persisted var emailAddress: String = ""
    @Persisted var profilePicURL: String = ""
    
    convenience init(givenName: String, lastName: String, emailAddress: String, profilePicURL: String) {
        self.init()
        self.givenName = givenName
        self.lastName = lastName
        self.emailAddress = emailAddress
        self.profilePicURL = profilePicURL
    }
    
    override func updateObject(withEntity entity: StorableObject) {
        guard let entity = entity as? User else { return }
        givenName = entity.givenName
        lastName = entity.lastName
        emailAddress = entity.emailAddress
        profilePicURL = entity.profilePicURL
    }
    
}

enum UserModelKey: String {
    case givenName
    case lastName
    case emailAddress
    case profilePicURL
    case userToken
}
