//
//  HomeViewController.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit
import Parchment

class HomeViewController: SharedViewController {
    
    private var viewControllers: [UIViewController]
    
    private lazy var pagingViewController: PagingViewController = {
        PagingViewController(viewControllers: viewControllers)
    }()
    
    init(viewControllers: [UIViewController]) {
        self.viewControllers = viewControllers
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPagingViewController()
    }
    
    func setupPagingViewController() {
        addChild(pagingViewController)
        view.addSubview(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        
        pagingViewController.menuBackgroundColor = GlobalSettings.shared().black ?? .black
        pagingViewController.borderColor = GlobalSettings.shared().darkGreen ?? .green
        pagingViewController.textColor = GlobalSettings.shared().white
        pagingViewController.selectedTextColor = GlobalSettings.shared().mainColor ?? .green
        pagingViewController.indicatorColor = GlobalSettings.shared().mainColor ?? .green
        pagingViewController.indicatorOptions = .visible(height: 5, zIndex: Int.max, spacing: .zero, insets: .zero)
        
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pagingViewController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            pagingViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pagingViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pagingViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
}
