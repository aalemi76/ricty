//
//  HomeCoordinator.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit
import Combine

class HomeCoordinator: Coordinator {
    
    var didFinish: PassthroughSubject<Coordinator, Never>
    
    var type: CoordinatorType
    
    var navigationController: UINavigationController
    
    var childCoordinators: [Coordinator]
    
    var cancellableStorage: Set<AnyCancellable>
    
    required init(type: CoordinatorType, navigationController: UINavigationController) {
        didFinish = PassthroughSubject()
        self.type = .tab
        self.navigationController = navigationController
        childCoordinators = []
        cancellableStorage = []
    }
    
    func start() {
        let listTypes = ListType.allCases
        let pagingViewControllers = listTypes.map { type in
            let coordinator = ListCoordinator(type: self.type, navigationController: RTNavigationController())
            coordinator.setupListType(type)
            coordinator.start()
            childCoordinators.append(coordinator)
            return coordinator.navigationController
        }
        let viewController = HomeViewController(viewControllers: pagingViewControllers)
        viewController.title = "Home"
        navigationController.setNavigationBarHidden(false, animated: false)
        push(viewController: viewController, animated: true)
    }
    
    
}

