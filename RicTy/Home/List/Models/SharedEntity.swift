//
//  SharedEntity.swift
//  RicTy
//
//  Created by AliReza on 2022-12-01.
//

import Foundation
import Combine
import RealmSwift

class SharedEntity: StorableObject, Codable {
    
    var didChangeFavoriteState = PassthroughSubject<Bool, Never>()
    
    @Persisted var identifier: Int?
    @Persisted var isFavorite: Bool = false {
        didSet {
            didChangeFavoriteState.send(isFavorite)
        }
    }
    
    override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        return
    }
    
    func encode(to encoder: Encoder) throws {
        return
    }
    
    func createObject() -> SharedEntity {
        return SharedEntity()
    }
    
    override func updateObject(withEntity entity: StorableObject) {
        guard let entity = entity as? SharedEntity else { return }
        _id = entity._id
        isFavorite = entity.isFavorite
    }
    
}
