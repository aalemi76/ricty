//
//  Location.swift
//  RicTy
//
//  Created by AliReza on 2022-12-01.
//

import Foundation

class Location: SharedEntity {
    
    var name, type, dimension: String?
    var residents: [String]?
    var url: String?
    var created: String?
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case name, type, dimension, residents, url, created
    }
    
    override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        type = try container.decode(String.self, forKey: .type)
        dimension = try container.decode(String.self, forKey: .dimension)
        residents = try container.decode([String].self, forKey: .residents)
        url = try container.decode(String.self, forKey: .url)
        created = try container.decode(String.self, forKey: .created)
        super.init()
        identifier = try container.decode(Int.self, forKey: .identifier)
    }
    
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(identifier, forKey:  .identifier)
        try container.encode(name, forKey: .name)
        try container.encode(type, forKey: .type)
        try container.encode(dimension, forKey: .dimension)
        try container.encode(residents, forKey: .residents)
        try container.encode(url, forKey: .url)
        try container.encode(created, forKey: .created)
    }
    
    override func createObject() -> Location {
        let location = Location()
        location.identifier = identifier
        location.name = name
        location.type = type
        location.dimension = dimension
        location.residents = residents
        location.url = url
        location.created = created
        location.isFavorite = isFavorite
        return location
    }
    
}
