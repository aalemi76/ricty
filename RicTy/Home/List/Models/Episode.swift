//
//  Episode.swift
//  RicTy
//
//  Created by AliReza on 2022-12-01.
//

import Foundation

class Episode: SharedEntity {
    
    var name, airDate, episode: String?
    var characters: [String]?
    var url: String?
    var created: String?
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case airDate = "air_date"
        case name, episode, characters, url, created
    }
    
    override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        airDate = try container.decode(String.self, forKey: .airDate)
        episode = try container.decode(String.self, forKey: .episode)
        characters = try container.decode([String].self, forKey: .characters)
        url = try container.decode(String.self, forKey: .url)
        created = try container.decode(String.self, forKey: .created)
        super.init()
        identifier = try container.decode(Int.self, forKey: .identifier)
    }
    
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(identifier, forKey:  .identifier)
        try container.encode(name, forKey: .name)
        try container.encode(airDate, forKey: .airDate)
        try container.encode(episode, forKey: .episode)
        try container.encode(characters, forKey: .characters)
        try container.encode(url, forKey: .url)
        try container.encode(created, forKey: .created)
    }
    
    override func createObject() -> Episode {
        let episode = Episode()
        episode.identifier = identifier
        episode.name = name
        episode.airDate = airDate
        episode.episode = self.episode
        episode.characters = characters
        episode.url = url
        episode.created = created
        episode.isFavorite = isFavorite
        return episode
    }
    
}
