//
//  Character.swift
//  RicTy
//
//  Created by AliReza on 2022-12-01.
//

import Foundation
import RealmSwift
import Combine

class Character: SharedEntity {
    
    var name: String?
    var status: Status?
    var species: String?
    var type: String?
    var gender: String?
    var origin, location: CharacterLocation?
    var image: String?
    var episodes: [String]?
    var url: String?
    var created: String?
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case name, status, species, type, gender, origin, location, image, url, created
        case episodes = "episode"
    }
    
    override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        status = try container.decode(Status.self, forKey: .status)
        species = try container.decode(String.self, forKey: .species)
        type = try container.decode(String.self, forKey: .type)
        gender = try container.decode(String.self, forKey: .gender)
        origin = try container.decode(CharacterLocation.self, forKey: .origin)
        location = try container.decode(CharacterLocation.self, forKey: .location)
        image = try container.decode(String.self, forKey: .image)
        episodes = try container.decode([String].self, forKey: .episodes)
        url = try container.decode(String.self, forKey: .url)
        created = try container.decode(String.self, forKey: .created)
        super.init()
        identifier = try container.decode(Int.self, forKey: .identifier)
    }

    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(identifier, forKey:  .identifier)
        try container.encode(name, forKey:  .name)
        try container.encode(status, forKey:  .status)
        try container.encode(species, forKey:  .species)
        try container.encode(type, forKey:  .type)
        try container.encode(gender, forKey:  .gender)
        try container.encode(origin, forKey:  .origin)
        try container.encode(location, forKey:  .location)
        try container.encode(image, forKey:  .image)
        try container.encode(episodes, forKey:  .episodes)
        try container.encode(url, forKey:  .url)
        try container.encode(created, forKey:  .created)

    }
    
    override func createObject() -> Character {
        let character = Character()
        character.identifier = identifier
        character.name = name
        character.status = status
        character.species = species
        character.type = type
        character.gender = gender
        character.origin = origin
        character.location = location
        character.image = image
        character.episodes = episodes
        character.url = url
        character.created = created
        character.isFavorite = isFavorite
        return character
    }
    
}

class CharacterLocation: SharedEntity {
    
    var name: String?
    var url: String?
    
    enum CodingKeys: CodingKey {
        case name, url
    }
    
    override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        url = try container.decode(String.self, forKey: .url)
        super.init()
    }
    
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(url, forKey: .url)
    }
    
}

enum Status: String, Codable {
    case alive = "Alive"
    case dead = "Dead"
    case unknown = "unknown"
}
