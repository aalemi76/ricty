//
//  SharedObject.swift
//  RicTy
//
//  Created by AliReza on 2022-12-01.
//

import Foundation

struct SharedObject<Model: Codable>: Codable {
    let info: Info
    let results: [Model]
}

struct Info: Codable {
    let count, pages: Int?
    let next: String?
    let prev: String?
}
