//
//  ListViewController+Ext.swift
//  RicTy
//
//  Created by AliReza on 2022-11-30.
//

import UIKit

extension ListViewController: Pageable {
    func show(result: Result<Any, RTError>) {
        
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let data):
                
                guard let sections = data as? [Sectionable] else {
                    self?.showErrorBanner(title: "An error occured while loading posts.")
                    return
                }
                self?.configureTableView(sections)
                
            case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
                self?.dismissLoadingView()
                if error == .internetConnectionError {
                    self?.connectionManager.startMonitoring()
                }
            }
            
        }
    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
        sinkWithLoadNextPage()
        sinkWithTapOnCells()
        sinkWithScrollView()
        dismissLoadingView()
    }
    
    func sinkWithLoadNextPage() {
        tableViewContainer.dataSourceHandler.loadNextPage.sink { [weak self] indexPath in
            self?.viewModel.loadNexPage(from: indexPath.section)
        }.store(in: &cancellableStorage)
    }
    
    func showNexPage(_ result: Result<Any, RTError>) {
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let data):
                
                guard let data = data as? ([Sectionable], IndexSet) else {
                    self?.showErrorBanner(title: "An error occured while loading posts.")
                    return}
                self?.loadNextPage(sections: data.0, sets: data.1)
                
            case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
                self?.dismissLoadingView()
            }
            
        }
    }
    
    func loadNextPage(sections: [Sectionable], sets: IndexSet) {
        tableViewContainer.addNewSections(sections, sets: sets)
    }
    
    func sinkWithTapOnCells() {
        tableViewContainer.delegateHandler.passSelectedItem.sink { model in
            
        }.store(in: &cancellableStorage)
    }
    
    func sinkWithScrollView() {
        tableViewContainer.delegateHandler.scrollViewDidScroll.sink { [weak self] srcollView in
            self?.scrollViewDidScroll(srcollView)
        }.store(in: &cancellableStorage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView?) {
        guard let scrollView = scrollView else { return }
        listHeaderView.scrollViewDidScroll(scrollView: scrollView)
//        if scrollView.contentOffset.y > 400 {
//            upBtn.isHidden = false
//        } else {
//            upBtn.isHidden = true
//        }
    }
    
    
}
