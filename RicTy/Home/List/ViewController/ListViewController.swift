//
//  ListViewController.swift
//  RicTy
//
//  Created by AliReza on 2022-11-30.
//

import UIKit

class ListViewController: SharedViewController {
    
    let viewModel: ListViewModelProvider
    
    let connectionManager = NetworkMonitor()
    
    lazy var listHeaderView: ListHeaderView = ListHeaderView(viewHeight: viewModel.type.getHeaderHeight(),
                                                             imageName: viewModel.type.getHeaderViewName())
    
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .insetGrouped)
        tbl.backgroundColor = .none
        tbl.tableHeaderView = listHeaderView
        tbl.tableFooterView = nil
        tbl.sectionHeaderHeight = 0
        tbl.sectionFooterHeight = 20
        tbl.separatorStyle = .none
        tbl.rowHeight = viewModel.type.getRowHeight()
        return tbl
    }()
    
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        return container
    }()
    
    init(viewModel: ListViewModelProvider) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        title = viewModel.type.rawValue
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeOnInternetConnection()
        addTableView()
        viewModel.viewDidLoad(self)
        showLoadingView()
        listHeaderView.setupLayer()
    }
    
    func addTableView() {
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }
    
    func observeOnInternetConnection() {
        connectionManager.didConnectedToInternet.sink { [unowned self] isConnected in
            guard isConnected else { return }
            connectionManager.stopMonitoring()
            viewModel.viewDidLoad(self)
        }.store(in: &cancellableStorage)
    }
    
}
