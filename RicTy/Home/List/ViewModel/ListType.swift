//
//  ListType.swift
//  RicTy
//
//  Created by AliReza on 2022-12-02.
//

import Foundation

enum ListType: String, CaseIterable {
    
    case characters = "Characters"
    
    case episodes = "Episodes"
    
    case locations = "Locations"
    
    func getRoute() -> Routes {
        switch self {
        case .characters:
            return Routes.characters
        case .episodes:
            return Routes.episodes
        case .locations:
            return Routes.locations
        }
    }
    
    func getCellReuseID() -> String {
        switch self {
        case .characters:
            return CharacterCell.reuseID
        case .episodes:
            return EpisodeCell.reuseID
        case .locations:
            return LocationCell.reuseID
        }
    }
    
    func getCellClass() -> AnyClass {
        switch self {
        case .characters:
            return CharacterCell.self
        case .episodes:
            return EpisodeCell.self
        case .locations:
            return LocationCell.self
        }
    }
    
    func getHeaderViewName() -> String {
        switch self {
        case .characters:
            return "CharactersHeader"
        case .episodes:
            return "EpisodesHeader"
        case .locations:
            return "LocationsHeader"
        }
    }
    
    func getHeaderHeight() -> CGFloat {
        return 230
    }
    
    func getRowHeight() -> CGFloat {
        switch self {
        case .characters:
            return 130
        case .episodes:
            return 200
        case .locations:
            return 200
        }
    }
    
}
