//
//  ListViewModelProvider.swift
//  RicTy
//
//  Created by AliReza on 2022-11-30.
//

import Foundation
import Combine

protocol ListViewModelProvider: ViewModelProvider {
    
    var showUIActivityViewController: PassthroughSubject<[Any], Never> { get }
    
    var type: ListType { get }
    
    init(type: ListType, interactor: Interactor)
    
    func loadNexPage(from section: Int)
    
}
