//
//  ListViewModel.swift
//  RicTy
//
//  Created by AliReza on 2022-11-30.
//

import Foundation
import Combine

class ListViewModel<Model: SharedEntity>: ListViewModelProvider {
    
    var showUIActivityViewController = PassthroughSubject<[Any], Never>()
    
    private let interactor: Interactor
    
    var type: ListType
    
    weak var view: Pageable?
    
    var page = 0
    
    var maxPage = 0
    
    var isFetchInProgress = false
    
    var cancellableStorage = Set<AnyCancellable>()
    
    required init(type: ListType, interactor: Interactor) {
        self.type = type
        self.interactor = interactor
    }
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view as? Pageable
        processData()
    }
    
    private func processData() {
        guard !isFetchInProgress else { return }
        page = 1
        isFetchInProgress = true
        interactor.getModel(type.getRoute(), parameters: ["page": page]) { [unowned self] (object: SharedObject<Model>) in
            maxPage = object.info.pages ?? 42
            var cells = [Reusable]()
            switch type {
            case .characters:
                cells = object.results.map({CharacterCellViewModel(reuseID: type.getCellReuseID(), cellClass: type.getCellClass(), model: $0)})
            case .episodes:
                cells = object.results.map({ EpisodeCellViewModel(reuseID: type.getCellReuseID(), cellClass: type.getCellClass(), model: $0)})
            case .locations:
                cells = object.results.map({ LocationCellViewModel(reuseID: type.getCellReuseID(), cellClass: type.getCellClass(), model: $0)})
            }
            cells.forEach({ sinkWithTapOnShareOption($0 as? ContextMenuProvider)} )
            let newSections = cells.map({ SectionProvider(title: nil, cells: [$0], headerView: nil, footerView: nil)})
            view?.show(result: .success(newSections))
            isFetchInProgress = false
        } onFailure: { [weak self] error in
            self?.view?.show(result: .failure(error))
            self?.isFetchInProgress = false
        }
    }
    
    func loadNexPage(from section: Int) {
        
        guard !isFetchInProgress && page <= maxPage else { return }
        
        page += 1
        
        isFetchInProgress = true
        interactor.getModel(type.getRoute(), parameters: ["page": page]) { [unowned self] (object: SharedObject<Model>) in
            maxPage = object.info.pages ?? 42
            var cells = [Reusable]()
            switch type {
            case .characters:
                cells = object.results.map({CharacterCellViewModel(reuseID: type.getCellReuseID(), cellClass: type.getCellClass(), model: $0)})
            case .episodes:
                cells = object.results.map({ EpisodeCellViewModel(reuseID: type.getCellReuseID(), cellClass: type.getCellClass(), model: $0)})
            case .locations:
                cells = object.results.map({ LocationCellViewModel(reuseID: type.getCellReuseID(), cellClass: type.getCellClass(), model: $0)})
            }
            cells.forEach({ sinkWithTapOnShareOption($0 as? ContextMenuProvider)} )
            let newSections = cells.map({ SectionProvider(title: nil, cells: [$0], headerView: nil, footerView: nil)})
            let range = section + 1 ... section + newSections.count
            let sets = IndexSet(integersIn: range)
            view?.showNexPage(.success((newSections, sets)))
            isFetchInProgress = false
        } onFailure: { [weak self] error in
            self?.page -= 1
            self?.view?.show(result: .failure(error))
            self?.isFetchInProgress = false
        }
    }
    
    func sinkWithTapOnShareOption(_ cellViewModel: ContextMenuProvider?) {
        cellViewModel?.didTapOnShareOption.sink(receiveValue: { [unowned self] model in
            
            var shareItems = [Any]()
            
            switch type {
                
            case .characters:
                guard let model = model as? Character else { return }
                shareItems = [model.name ?? "", model.status ?? ""]
                if let url = URL(string: model.image ?? "") {
                    shareItems.append(url)
                }
                
            case .episodes:
                guard let model = model as? Episode else { return }
                shareItems = [model.name ?? "", model.episode ?? "", model.airDate ?? ""]
                
            case .locations:
                guard let model = model as? Location else { return }
                shareItems = [model.name ?? "", model.type ?? "", model.dimension ?? ""]
            }
            showUIActivityViewController.send(shareItems)
        }).store(in: &cancellableStorage)
    }
    
}
