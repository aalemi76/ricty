//
//  CharacterCellViewModel.swift
//  RicTy
//
//  Created by AliReza on 2022-12-01.
//

import Foundation
import Combine

class CharacterCellViewModel: SharedCellViewModel, ContextMenuProvider {
    
    var didTapOnShareOption = PassthroughSubject<SharedEntity, Never>()
    
    let interactor = Interactor()
    
    weak var cell: Updatable?
    
    var cancellableStorage = Set<AnyCancellable>()
    
    override func cellDidLoad(_ cell: Updatable) {
        self.cell = cell
        updateModelWithDB()
        (model as? Character)?.didChangeFavoriteState.sink { [weak self] newValue in
            (self?.cell as? CharacterCell)?.didUpdateFavoriteState(newValue: newValue)
        }.store(in: &cancellableStorage)
    }
    
    func didTapOnFavoriteButton() {
        guard let model = model as? Character else { return }
        interactor.updateDBObject(model, withPredicate: NSPredicate(format: "identifier == %i", model.identifier ?? -2)) { [weak self] dbEntity in
            if !model.isInvalidated, let dbEntity = dbEntity {
                model.isFavorite = !model.isFavorite
                dbEntity.isFavorite = model.isFavorite
            } else {
                model.isFavorite = true
                let character = model.createObject()
                self?.interactor.addEntity(character)
            }
        }
    }
    
    private func updateModelWithDB() {
        guard let model = model as? Character else { return }
        interactor.updateFetchedObject(model, withPredicate: NSPredicate(format: "identifier == %i", model.identifier ?? -2))
    }
    
    func didForceTouchOnCell() -> SharedEntity? {
        return model as? SharedEntity
    }
    
    func didTapOnFavoriteOption() {
        didTapOnFavoriteButton()
    }
    
}
