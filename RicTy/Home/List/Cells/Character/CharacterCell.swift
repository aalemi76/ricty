//
//  CharacterCell.swift
//  RicTy
//
//  Created by AliReza on 2022-12-01.
//

import UIKit
import SDWebImage

class CharacterCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: CharacterCell.self)
    
    var viewModel: CharacterCellViewModel?
    
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var speciesLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var originLabel: UILabel!
    
    @IBOutlet weak var characterImageView: UIImageView!
    
    @IBOutlet weak var favoriteButton: UIButton!
    
    @IBAction func didTapOnFavoriteButton(_ sender: UIButton) {
        viewModel?.didTapOnFavoriteButton()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = true
        layer.cornerRadius = 10
        statusView.layer.masksToBounds = true
        statusView.layer.cornerRadius = statusView.frame.width/2
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel as? CharacterCellViewModel
        self.viewModel?.cellDidLoad(self)
    }
    
    func update(model: Any) {
        guard let model = model as? Character else { return }
        characterImageView.sd_setImage(with: URL(string: model.image ?? ""))
        nameLabel.text = model.name
        statusLabel.text = model.status?.rawValue
        speciesLabel.text = model.species
        locationLabel.text = model.location?.name
        originLabel.text = model.origin?.name
        didUpdateFavoriteState(newValue: model.isFavorite)
        if let status = model.status {
            switch status {
            case .alive:
                statusView.backgroundColor = GlobalSettings.shared().lightGreen
            case .dead:
                statusView.backgroundColor = GlobalSettings.shared().lightRed
            case .unknown:
                statusView.backgroundColor = .gray
            }
        }
    }
    
    func didUpdateFavoriteState(newValue: Bool) {
        favoriteButton.setTitle("", for: .normal)
        if newValue {
            favoriteButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        } else {
            favoriteButton.setImage(UIImage(systemName: "star"), for: .normal)
        }
    }
    
}
