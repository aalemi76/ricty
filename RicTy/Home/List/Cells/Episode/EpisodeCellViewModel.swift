//
//  EpisodeCellViewModel.swift
//  RicTy
//
//  Created by AliReza on 2022-12-05.
//

import Foundation
import Combine

class EpisodeCellViewModel: SharedCellViewModel, ContextMenuProvider {
    
    var didTapOnShareOption = PassthroughSubject<SharedEntity, Never>()
    
    
    let interactor = Interactor()
    
    weak var cell: Updatable?
    
    var cancellableStorage = Set<AnyCancellable>()
    
    override func cellDidLoad(_ cell: Updatable) {
        self.cell = cell
        updateModelWithDB()
        (model as? Episode)?.didChangeFavoriteState.sink { [weak self] newValue in
            (self?.cell as? EpisodeCell)?.didUpdateFavoriteState(newValue: newValue)
        }.store(in: &cancellableStorage)
        fetchCharcters()
    }
    
    func fetchCharcters() {
        guard let model = model as? Episode else { return }
        let ids = getCharacterIDs(model)
        let route = Routes.characters
        interactor.getModel(route, parameters: ["ids": ids]) { [weak self] (characters: [Character]) in
            let cells = characters.map({SharedCellViewModel(reuseID: ImageCollectionViewCell.reuseID, cellClass: ImageCollectionViewCell.self, model: $0)})
            let sections = [SectionProvider(title: nil, cells: cells, headerView: nil, footerView: nil)]
            (self?.cell as? Viewable)?.show(result: .success(sections))
        } onFailure: { [weak self] error in
            (self?.cell as? Viewable)?.show(result: .failure(error))
        }

    }
    
    private func getCharacterIDs(_ model: Episode) -> String {
        var characters = ""
        for character in model.characters ?? [] {
            if let characterID = character.split(separator: "/").last {
                characters += "\(characterID),"
            }
        }
        return String(characters.dropLast())
    }
    
    func didTapOnFavoriteButton() {
        guard let model = model as? Episode else { return }
        
        interactor.updateDBObject(model, withPredicate: NSPredicate(format: "identifier == %i", model.identifier ?? -2)) { [weak self] dbEntity in
            if !model.isInvalidated, let dbEntity = dbEntity {
                model.isFavorite = !model.isFavorite
                dbEntity.isFavorite = model.isFavorite
            } else {
                model.isFavorite = true
                let entity = model.createObject()
                self?.interactor.addEntity(entity)
            }
        }
    }
    
    private func updateModelWithDB() {
        guard let model = model as? Episode else { return }
        interactor.updateFetchedObject(model, withPredicate: NSPredicate(format: "identifier == %i", model.identifier ?? -2))
    }
    
    func didForceTouchOnCell() -> SharedEntity? {
        return model as? SharedEntity
    }
    
    func didTapOnFavoriteOption() {
        didTapOnFavoriteButton()
    }
    
}
