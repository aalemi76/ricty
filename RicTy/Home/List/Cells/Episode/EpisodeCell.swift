//
//  EpisodeCell.swift
//  RicTy
//
//  Created by AliReza on 2022-12-05.
//

import UIKit

class EpisodeCell: UITableViewCell, Updatable, Viewable {
    
    static let reuseID = String(describing: EpisodeCell.self)
    
    var viewModel: EpisodeCellViewModel?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dimensionLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 100, height: 100)
        layout.minimumLineSpacing = 20
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .none
        return view
    }()
    
    lazy var collectionViewContainer: CollectionViewContainer = {
        let container = CollectionViewContainer(collectionView: collectionView)
        return container
    }()

    @IBAction func didTapOnFavoriteButton(_ sender: UIButton) {
        viewModel?.didTapOnFavoriteButton()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = true
        layer.cornerRadius = 10
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionViewContainer.translatesAutoresizingMaskIntoConstraints = false
        addSubview(collectionViewContainer)
        NSLayoutConstraint.activate([
            collectionViewContainer.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            collectionViewContainer.topAnchor.constraint(equalTo: favoriteButton.bottomAnchor, constant: 10),
            collectionViewContainer.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            collectionViewContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5)])
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel as? EpisodeCellViewModel
        self.viewModel?.cellDidLoad(self)
    }
    
    func update(model: Any) {
        guard let model = model as? Episode else { return }
        nameLabel.text = model.name
        typeLabel.text = model.episode
        dimensionLabel.text = model.airDate
        didUpdateFavoriteState(newValue: model.isFavorite)
        viewModel?.fetchCharcters()
        
    }
    
    func didUpdateFavoriteState(newValue: Bool) {
        favoriteButton.setTitle("", for: .normal)
        if newValue {
            favoriteButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        } else {
            favoriteButton.setImage(UIImage(systemName: "star"), for: .normal)
        }
    }
    
    func show(result: Result<Any, RTError>) {
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let data):
                guard let sections = data as? [Sectionable] else {
                    return
                }
                self?.configureCollectionView(sections)
            case .failure:
                self?.collectionViewContainer.isHidden = true
                return
            }
        }
    }
    
    func configureCollectionView(_ sections: [Sectionable]) {
        collectionViewContainer.isHidden = false
        collectionViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
    }

    
}
