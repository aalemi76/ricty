//
//  ImageCollectionViewCell.swift
//  RicTy
//
//  Created by AliReza on 2022-12-05.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell, Updatable {
    
    static let reuseID = String(describing: ImageCollectionViewCell.self)
    
    @IBOutlet weak var imageView: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = true
        layer.cornerRadius = 20
        layer.borderWidth = 1
        layer.borderColor = GlobalSettings.shared().darkGreen?.cgColor
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        guard let model = model as? Character else { return }
        imageView?.sd_setImage(with: URL(string: model.image ?? ""))
    }

}
