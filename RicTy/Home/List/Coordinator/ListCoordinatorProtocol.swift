//
//  ListCoordinatorProtocol.swift
//  RicTy
//
//  Created by AliReza on 2022-11-30.
//

import Foundation

protocol ListCoordinatorProtocol: Coordinator {
    
    var listType: ListType { get }
    
    func setupListType(_ type: ListType)
    
    func showDetailViewController(id: Int)
    
}
