//
//  ListCoordinator.swift
//  RicTy
//
//  Created by AliReza on 2022-11-30.
//

import UIKit
import Combine

class ListCoordinator: ListCoordinatorProtocol {
    
    
    var didFinish: PassthroughSubject<Coordinator, Never>
    
    var type: CoordinatorType
    
    var navigationController: UINavigationController
    
    var childCoordinators: [Coordinator]
    
    var cancellableStorage: Set<AnyCancellable>
    
    var listType: ListType = .characters
    
    required init(type: CoordinatorType, navigationController: UINavigationController) {
        didFinish = PassthroughSubject()
        self.type = type
        self.navigationController = navigationController
        self.navigationController.setNavigationBarHidden(true, animated: false)
        childCoordinators = []
        cancellableStorage = []
    }
    
    func start() {
        let viewController = createListViewController(type: listType)
        push(viewController: viewController, animated: true)
    }
    
    private func createListViewController(type: ListType) -> SharedViewController {
        switch type {
            
        case .characters:
            let viewModel = ListViewModel<Character>(type: type, interactor: Interactor())
            showUIActivityViewController(viewModel: viewModel)
            let viewController = ListViewController(viewModel: viewModel)
            return viewController
        case .episodes:
            let viewModel = ListViewModel<Episode>(type: type, interactor: Interactor())
            showUIActivityViewController(viewModel: viewModel)
            let viewController = ListViewController(viewModel: viewModel)
            return viewController
        case .locations:
            let viewModel = ListViewModel<Location>(type: type, interactor: Interactor())
            showUIActivityViewController(viewModel: viewModel)
            let viewController = ListViewController(viewModel: viewModel)
            return viewController
        }
    }
    
    func setupListType(_ type: ListType) {
        self.listType = type
    }
    
    func showDetailViewController(id: Int) {
        return
    }
    
    func showUIActivityViewController(viewModel: some ListViewModelProvider) {
        viewModel.showUIActivityViewController.sink { [weak self] shareItems in
            let viewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            self?.present(viewController: viewController, animated: true)
        }.store(in: &cancellableStorage)
    }
    
    
}
