//
//  ProfileViewController+Ext.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import Foundation

extension ProfileViewController: Viewable {
    
    func show(result: Result<Any, RTError>) {
        
        switch result {
            
        case .success(let sections):
            
            guard let sections = sections as? [Sectionable] else {
                showErrorBanner(title: RTError.cannotRetrieveUser.rawValue)
                return
            }
            
            configureTableView(sections)
            
        case .failure(let error):
            
            showErrorBanner(title: error.rawValue)
        }
    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
    }
    
}
