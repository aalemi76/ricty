//
//  UserInfoCell.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import UIKit
import SDWebImage

enum UserInfoCellType: String {
    case imagePicURL
    case givenName
    case lastName
    case email
}

class UserInfoCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: UserInfoCell.self)
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        
        guard let model = model as? (UserInfoCellType, String) else { return }
        
        setupView(cellType: model.0, info: model.1)
        
    }
    
    func setupView(cellType: UserInfoCellType, info: String) {
        switch cellType {
            
        case .imagePicURL:
            addProfilePic(url: info)
        case .givenName:
            addInfoLabels(title: cellType.rawValue.capitalized, content: info)
        case .lastName:
            addInfoLabels(title: cellType.rawValue.capitalized, content: info)
        case .email:
            addInfoLabels(title: cellType.rawValue.capitalized, content: info)
        }
    }
    
    func addProfilePic(url: String) {
        
        let profilePicView: UIImageView = {
            let view = UIImageView()
            view.contentMode = .scaleAspectFill
            view.clipsToBounds = true
            view.layer.cornerRadius = 100
            view.image = UIImage(systemName: "person.crop.circle.fill.badge.plus")
            view.tintColor = GlobalSettings.shared().mainColor
            return view
        }()
        
        profilePicView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(profilePicView)
        NSLayoutConstraint.activate([
            profilePicView.widthAnchor.constraint(equalToConstant: 200),
            profilePicView.heightAnchor.constraint(equalToConstant: 200),
            profilePicView.centerXAnchor.constraint(equalTo: centerXAnchor),
            profilePicView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            profilePicView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)])
        
        profilePicView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(systemName: "person.crop.circle.fill.badge.plus"))
        
    }
    
    func addInfoLabels(title: String, content: String) {
        
        let contentLabel = UILabel()
        contentLabel.text = content
        contentLabel.font = GlobalSettings.shared().systemFont(type: .light, size: 18)
        contentLabel.textColor = GlobalSettings.shared().mainColor
        contentLabel.numberOfLines = 0
        contentLabel.lineBreakMode = .byWordWrapping
        contentLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(contentLabel)
        
        NSLayoutConstraint.activate([
            contentLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            contentLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            contentLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            contentLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        ])
        
    }
    
}
