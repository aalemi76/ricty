//
//  LoginButtonCell.swift
//  RicTy
//
//  Created by AliReza on 2022-11-30.
//

import UIKit

class LoginButtonCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: LoginButtonCell.self)
    
    var viewModel: Reusable?
    
    private lazy var loginButton: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = GlobalSettings.shared().mainColor
        btn.setTitleColor(GlobalSettings.shared().black, for: .normal)
        btn.titleLabel?.font = GlobalSettings.shared().systemFont(type: .bold, size: 18)
        return btn
    }()
    
    @objc func didTapOnLoginButton() {
        (viewModel as? LoginButtonCellViewModel)?.loginButtonTouchUpInside()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel
        self.viewModel?.cellDidLoad(self)
    }
    
    func update(model: Any) {
        addLoginButton()
    }
    
    func didUpdateLoginState(_ newState: LoginState) {
        loginButton.setTitle(newState.rawValue, for: .normal)
    }
    
    func addLoginButton() {
        
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(loginButton)
        NSLayoutConstraint.activate([
            loginButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            loginButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            loginButton.heightAnchor.constraint(equalToConstant: 40),
            loginButton.widthAnchor.constraint(equalToConstant: 100),
            loginButton.topAnchor.constraint(equalTo: topAnchor, constant: 10)])
        loginButton.layer.cornerRadius = 15
        
        loginButton.addTarget(self, action: #selector(didTapOnLoginButton), for: .touchUpInside)
        
        loginButton.setTitle((viewModel as? LoginButtonCellViewModel)?.state.rawValue, for: .normal)
        
    }
    
}
