//
//  LoginButtonCellViewModel.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import Foundation
import Combine

enum LoginState: String {
    case login = "Log Out"
    case logout = "Login"
}

class LoginButtonCellViewModel: SharedCellViewModel {
    
    var didTapOnLoginButton = PassthroughSubject<LoginState, Never>()
    
    weak var cell: Updatable?
    
    var state: LoginState {
        didSet {
            didUpdateLoginState(state)
        }
    }
    
    required init(reuseID: String, cellClass: AnyClass, model: Any) {
        if UserDefaultsManager.shared.isSignedIn {
            state = .login
        } else {
            state = .logout
        }
        super.init(reuseID: reuseID, cellClass: cellClass, model: model)
    }
    
    override func cellDidLoad(_ cell: Updatable) {
        self.cell = cell
    }
    
    func loginButtonTouchUpInside() {
        didTapOnLoginButton.send(state)
    }
    
    func didUpdateLoginState(_ newState: LoginState) {
        (cell as? LoginButtonCell)?.didUpdateLoginState(newState)
    }
    
}
