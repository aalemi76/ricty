//
//  ProfileViewModel.swift
//  RicTy
//
//  Created by AliReza on 2022-11-29.
//

import Foundation
import Combine

class ProfileViewModel: ViewModelProvider {
    
    weak var view: Viewable?
    
    var cancellableStorage = Set<AnyCancellable>()
    
    var showLoginFlow: PassthroughSubject<Bool, Never> = PassthroughSubject()
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        setupTableView()
    }
    
    func setupTableView() {
        
        var sections = [Sectionable]()
        
        if let user = UserDefaultsManager.shared.getUser() {
            
            let profilePicSection = SectionProvider(title: nil, cells: [SharedCellViewModel(reuseID: UserInfoCell.reuseID, cellClass: UserInfoCell.self, model: (UserInfoCellType.imagePicURL, user.profilePicURL))], headerView: nil, footerView: nil)
            
            let givenNameSection = SectionProvider(title: "First Name", cells: [SharedCellViewModel(reuseID: UserInfoCell.reuseID, cellClass: UserInfoCell.self, model: (UserInfoCellType.givenName, user.givenName))], headerView: nil, footerView: nil)
            
            let lastNameSection = SectionProvider(title: "Last Name", cells: [SharedCellViewModel(reuseID: UserInfoCell.reuseID, cellClass: UserInfoCell.self, model: (UserInfoCellType.lastName, user.lastName))], headerView: nil, footerView: nil)
            
            let emailSection = SectionProvider(title: "Email address", cells: [SharedCellViewModel(reuseID: UserInfoCell.reuseID, cellClass: UserInfoCell.self, model: (UserInfoCellType.email, user.emailAddress))], headerView: nil, footerView: nil)
            
            sections = [profilePicSection, givenNameSection, lastNameSection, emailSection]
        }
        
        let loginCellViewModel = LoginButtonCellViewModel(reuseID: LoginButtonCell.reuseID, cellClass: LoginButtonCell.self, model: "")
        
        sinkWithLoginButton(viewModel: loginCellViewModel)
        
        let loginButtonSection = SectionProvider(title: nil, cells: [loginCellViewModel], headerView: nil, footerView: nil)
        
        sections.append(loginButtonSection)
        
        view?.show(result: .success(sections))
    }
    
    func sinkWithLoginButton(viewModel: LoginButtonCellViewModel) {
        viewModel.didTapOnLoginButton.sink { [weak self] state in
            switch state {
                
            case .login:
                if let user = UserDefaultsManager.shared.getUser() {
                    let name = "\(user.givenName) \(user.lastName)"
                    (self?.view as? ProfileViewController)?.showAlert(name: name)
                }
            case .logout:
                self?.showLoginFlow.send(true)
            }
        }.store(in: &cancellableStorage)
    }
    
    func didTapOnLogOut() {
        UserDefaultsManager.shared.signOut()
        setupTableView()
    }
}
