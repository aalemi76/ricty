//
//  ProfileCoordinator.swift
//  RicTy
//
//  Created by AliReza on 2022-11-28.
//

import UIKit
import Combine

class ProfileCoordinator: Coordinator {
    
    var didFinish: PassthroughSubject<Coordinator, Never>
    
    var type: CoordinatorType
    
    var navigationController: UINavigationController
    
    var childCoordinators: [Coordinator]
    
    var cancellableStorage: Set<AnyCancellable>
    
    required init(type: CoordinatorType, navigationController: UINavigationController) {
        didFinish = PassthroughSubject()
        self.type = type
        self.navigationController = navigationController
        childCoordinators = []
        cancellableStorage = []
    }
    
    func start() {
        let viewModel = ProfileViewModel()
        viewModel.showLoginFlow.sink { [unowned self] _ in
            self.didFinish.send(self)
        }.store(in: &cancellableStorage)
        let viewController = ProfileViewController(viewModel: viewModel)
        viewController.title = "Profile"
        navigationController.setNavigationBarHidden(false, animated: false)
        push(viewController: viewController, animated: true)
    }
    
    
}
