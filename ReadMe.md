# RADAR

## Overview
<img src="https://img.shields.io/badge/Platform-iOS-blueviolet">
<img src="https://img.shields.io/badge/Language-Swift-important">
<img src="https://img.shields.io/badge/iOS-13%2B-yellow">


The RicTy is an application based on the television show Rick and Morty. You will have access to about hundreds of characters, their images, locations and episodes. The RicTy app is filled with canonical information as seen on the TV show. Below you can see a snapshot of the application.

<center><img src="appScreenshot.png" width="332.70" height="720"></center>

## Architecture

The app's architecture is MVVM-C. There is a model associated with every character, location, and episode, which stores available properties of that entity, such as name, state, identifier, and etc. There is an Interactor which communicate with Database and Backend API to recieve data and parse them to appropiriate objects. ViewModel owns this class instance and observes its changes to update the coresponding view controller. ViewControllers only display UI components and receive user interaction and pass it to ViewModel. The coordinator in this app takes care of navigating between different parts of the applications. They create ViewControllers and also observe them if needed.


